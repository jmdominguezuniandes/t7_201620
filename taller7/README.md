�Es posible lograr el mismo resultado con el pre-orden y pos-orden?

No es posible, puesto que al tener los string pos-orden y pre-orden
al momento de reconstruir el �rbol se pueden generar soluciones ambiguas
puesto que no se puede determinar realmente en orden en el cual se
a�adieron los elementos al �rbol, por lo cual m�s de un �rbol puede
ser generado teniendo un pre orden y un post orden.

�Es posible lograr el mismo resultado con el in-orden y el pos-orden?
Si es posible, puesto que al igual que con el pre-orden todo �rbol binario
tiene una forma �nica dado un in-orden y un pos-orden. Es decir que no
existen soluciones ambiguas al momento de reconstruir el �rbol, permitiendo
que se reconstruya de la �nica forma posible.