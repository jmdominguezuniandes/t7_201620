package estructuras;
import estructuras.Queue;

public class Arbol <K extends Comparable, V> {

	private Nodo raiz;
	private String inOrden;
	private String preOrden;
	
	public Arbol(){
		raiz = null;
	}
	
	public void reconstruirArbol(String[] pInOrden, String[] pPreOrden, String pPre, String pIn){
		//Se crea el primer nodo
		inOrden = pIn;
		preOrden = pPre;
		raiz = new Nodo(pPreOrden[0],pPreOrden[0]);
		boolean raizEncontrada = false;
		Queue izquierda = new Queue();
		Queue derecha = new Queue();
		for(int i = 0; i < pInOrden.length; i++){
			if(pPreOrden[0].equals(pInOrden[i])){
				raizEncontrada = true;
			}
			else if(raizEncontrada == false){
				izquierda.enqueue(pInOrden[i]);
				
			}
			else{
				derecha.enqueue(pInOrden[i]);
			}
		}
		
		raiz.reconstruirSubArbol(derecha, izquierda, pPreOrden);
		
	}
	
	public String darPreOrden(){
		String preOrden = "";
		String respuesta = raiz.darPreOrden(preOrden);
		return respuesta;
	}
	
	public String darInOrden(){
		String inOrden = "";
		String respuesta = raiz.darInOrden(inOrden);
		return respuesta;
	}

	public boolean contieneSubArbol( String pSubPre, String pSubIn) {
		// TODO Auto-generated method stub
		if(inOrden.contains(pSubIn) && preOrden.contains(pSubPre)){
			return true;
		}
		return false;
	}
}
