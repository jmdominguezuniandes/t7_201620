package estructuras;

public class Nodo <K extends Comparable, V> {

	private K llave;
	private V valor;
	private Nodo izquierdo;
	private Nodo derecho;
	
	public Nodo(K pLlave, V pValor){
		llave = pLlave;
		valor = pValor;
		izquierdo = null;
		derecho = null;
		
	}
	
	public V darValor(){
		return valor;
	}
	
	public K darLlave(){
		return llave;
	}
	
	public boolean esHoja(){
		if (izquierdo == null && derecho == null){
			return true;
		}
		else{
			return false;
		}
	}
	
	public V buscar(K pLlave){
		if(pLlave.equals(llave)){
			return valor;
		}
		else if(llave.compareTo(pLlave) > 0){
			return (izquierdo == null) ? null: (V) izquierdo.buscar(pLlave);
		}
		else{
			return (derecho == null) ? null: (V) derecho.buscar(pLlave);
		}
	}
	
	public void reconstruirSubArbol(Queue pDerecha, Queue pIzquierda, K[] preOrden){
		Queue colaDerecha = pDerecha;
		Queue colaIzquierda = pIzquierda;
		Object elementosDerecha[] = new Object[pDerecha.getSize()];
		Object elementosIzquierda[] = new Object[pIzquierda.getSize()];
		for(int i = 0; i < elementosDerecha.length; i++){
			elementosDerecha[i] = colaDerecha.dequeue();
		}
		for(int i = 0; i < elementosIzquierda.length; i++){
			elementosIzquierda[i] = colaIzquierda.dequeue();
		}
		K elementoNodoIzq = (K) PrimerElemento(elementosIzquierda, preOrden);
		if(elementoNodoIzq != null){
			izquierdo = new Nodo(elementoNodoIzq,elementoNodoIzq);
			boolean raizEncontrada = false;
			Queue izquierda = new Queue();
			Queue derecha = new Queue();
			for(int i = 0; i < elementosIzquierda.length; i++){
				if(elementoNodoIzq.equals(elementosIzquierda[i])){
					raizEncontrada = true;
				}
				else if(raizEncontrada == false){
					izquierda.enqueue(elementosIzquierda[i]);
				}
				else{
					derecha.enqueue(elementosIzquierda[i]);
				}
			}
			izquierdo.reconstruirSubArbol(derecha, izquierda, preOrden);
		}
		
		K elementoNodoDer = (K) PrimerElemento(elementosDerecha, preOrden);
		if(elementoNodoDer != null){
			derecho = new Nodo(elementoNodoDer,elementoNodoDer);
			boolean raizEncontrada = false;
			Queue izquierda1 = new Queue();
			Queue derecha1 = new Queue();
			for(int i = 0; i < elementosDerecha.length; i++){
				if(elementoNodoDer.equals(elementosDerecha[i])){
					raizEncontrada = true;
				}
				else if(raizEncontrada == false){
					izquierda1.enqueue(elementosDerecha[i]);
				}
				else{
					derecha1.enqueue(elementosDerecha[i]);
				}
			}
			derecho.reconstruirSubArbol(derecha1, izquierda1, preOrden);
		}
		
		return;


		
	}
	
	/**
	 * Método que verifica de los elementos de la lista 1, cual es el que está primero en la lista 2
	 * @param lista1
	 * @param lista2
	 * @return
	 */
	public Object PrimerElemento(Object[] lista1, K[] lista2){
		int menorDistancia = lista2.length;
		Object primerElemento = null;
		for(int i = 0; i < lista1.length; i++){
			Object elementoAVerificar = lista1[i];
			int distancia = 0;
			for(int j = 0; j < lista2.length; j++){
				if(lista2[j].equals((K) elementoAVerificar)){
					break;
				}
				distancia ++;
			}
			if (distancia < menorDistancia){
				menorDistancia = distancia;
				primerElemento = elementoAVerificar;
			}
		}
		return primerElemento;
	}
	
	public String darPreOrden(String pRespuesta){
		
		pRespuesta = pRespuesta + (String) valor;
		if(izquierdo != null){
			pRespuesta = izquierdo.darPreOrden(pRespuesta);
			
		}
		
		if (derecho != null){
			pRespuesta = derecho.darPreOrden(pRespuesta);
		}
		
		return pRespuesta;
	}
	
	public String darInOrden(String pRespuesta){
		if(izquierdo != null){
			pRespuesta = izquierdo.darInOrden(pRespuesta);
			
		}
		
		pRespuesta = pRespuesta + (String) valor;
		
		if (derecho != null){
			pRespuesta = derecho.darInOrden(pRespuesta);
		}
		
		return pRespuesta;
	}

}

