package taller.test;

import junit.framework.TestCase;
import estructuras.Arbol;

public class SiembraTest extends TestCase {

	private Arbol arbol;
	
	public void setupEscenario1(){
		arbol = new Arbol();
	}
	
	public void testReconstruir1(){
		setupEscenario1();
	    String[] preOrden = new String[6];
	    String[] inOrden = new String[6];
	    preOrden[0] = "U";
	    preOrden[1] = "A";
	    preOrden[2] = "D";
	    preOrden[3] = "E";
	    preOrden[4] = "N";
	    preOrden[5] = "S";
	    inOrden[0] = "D";
	    inOrden[1] = "A";
	    inOrden[2] = "E";
	    inOrden[3] = "U";
	    inOrden[4] = "N";
	    inOrden[5] = "S";
	    
	    arbol.reconstruirArbol(inOrden, preOrden, "UADENS","DAEUNS");
	    String pre = arbol.darPreOrden();
	    String in = arbol.darInOrden();
	    //Se revisa si el preorden dado por el arbol es igual al insertado
	    assertEquals("El primer elemento debería ser U",'U', pre.charAt(0));
	    assertEquals("El primer elemento debería ser A",'A', pre.charAt(1));
	    assertEquals("El primer elemento debería ser D",'D', pre.charAt(2));
	    assertEquals("El primer elemento debería ser E",'E', pre.charAt(3));
	    assertEquals("El primer elemento debería ser N",'N', pre.charAt(4));
	    assertEquals("El primer elemento debería ser S",'S', pre.charAt(5));
	    //Se revisa si el inorden dado por el arbol es igual al insertado
	    assertEquals("El primer elemento debería ser D",'D', in.charAt(0));
	    assertEquals("El primer elemento debería ser A",'A', in.charAt(1));
	    assertEquals("El primer elemento debería ser E",'E', in.charAt(2));
	    assertEquals("El primer elemento debería ser U",'U', in.charAt(3));
	    assertEquals("El primer elemento debería ser N",'N', in.charAt(4));
	    assertEquals("El primer elemento debería ser S",'S', in.charAt(5));
	    
	}
	
	public void testReconstruir2(){
		setupEscenario1();
	    String[] preOrden = new String[7];
	    String[] inOrden = new String[7];
	    preOrden[0] = "a";
	    preOrden[1] = "b";
	    preOrden[2] = "d";
	    preOrden[3] = "e";
	    preOrden[4] = "c";
	    preOrden[5] = "f";
	    preOrden[6] = "g";
	    inOrden[0] = "d";
	    inOrden[1] = "b";
	    inOrden[2] = "e";
	    inOrden[3] = "a";
	    inOrden[4] = "f";
	    inOrden[5] = "c";
	    inOrden[6] = "g";
	    
	    arbol.reconstruirArbol(inOrden, preOrden, "abdecfg", "dbeafcg");
	    String pre = arbol.darPreOrden();
	    String in = arbol.darInOrden();
	    //Se revisa si el preorden dado por el arbol es igual al insertado
	    assertEquals("El primer elemento debería ser a",'a', pre.charAt(0));
	    assertEquals("El primer elemento debería ser b",'b', pre.charAt(1));
	    assertEquals("El primer elemento debería ser d",'d', pre.charAt(2));
	    assertEquals("El primer elemento debería ser e",'e', pre.charAt(3));
	    assertEquals("El primer elemento debería ser c",'c', pre.charAt(4));
	    assertEquals("El primer elemento debería ser f",'f', pre.charAt(5));
	    assertEquals("El primer elemento debería ser g",'g', pre.charAt(6));
	    //Se revisa si el inorden dado por el arbol es igual al insertado
	    assertEquals("El primer elemento debería ser d",'d', in.charAt(0));
	    assertEquals("El primer elemento debería ser b",'b', in.charAt(1));
	    assertEquals("El primer elemento debería ser e",'e', in.charAt(2));
	    assertEquals("El primer elemento debería ser a",'a', in.charAt(3));
	    assertEquals("El primer elemento debería ser f",'f', in.charAt(4));
	    assertEquals("El primer elemento debería ser c",'c', in.charAt(5));
	    assertEquals("El primer elemento debería ser g",'g', pre.charAt(6));
	    
	}
	
	public void testReconstruir3(){
		setupEscenario1();
	    String[] preOrden = new String[7];
	    String[] inOrden = new String[7];
	    preOrden[0] = "j";
	    preOrden[1] = "k";
	    preOrden[2] = "l";
	    preOrden[3] = "o";
	    preOrden[4] = "q";
	    preOrden[5] = "h";
	    preOrden[6] = "p";
	    inOrden[0] = "k";
	    inOrden[1] = "l";
	    inOrden[2] = "j";
	    inOrden[3] = "h";
	    inOrden[4] = "q";
	    inOrden[5] = "o";
	    inOrden[6] = "p";
	    
	    arbol.reconstruirArbol(inOrden, preOrden, "jkloqhp", "kljhqop");
	    String pre = arbol.darPreOrden();
	    String in = arbol.darInOrden();
	    //Se revisa si el preorden dado por el arbol es igual al insertado
	    assertEquals("El primer elemento debería ser j",'j', pre.charAt(0));
	    assertEquals("El primer elemento debería ser k",'k', pre.charAt(1));
	    assertEquals("El primer elemento debería ser l",'l', pre.charAt(2));
	    assertEquals("El primer elemento debería ser o",'o', pre.charAt(3));
	    assertEquals("El primer elemento debería ser q",'q', pre.charAt(4));
	    assertEquals("El primer elemento debería ser h",'h', pre.charAt(5));
	    assertEquals("El primer elemento debería ser p",'p', pre.charAt(6));
	    //Se revisa si el inorden dado por el arbol es igual al insertado
	    assertEquals("El primer elemento debería ser k",'k', in.charAt(0));
	    assertEquals("El primer elemento debería ser l",'l', in.charAt(1));
	    assertEquals("El primer elemento debería ser j",'j', in.charAt(2));
	    assertEquals("El primer elemento debería ser h",'h', in.charAt(3));
	    assertEquals("El primer elemento debería ser q",'q', in.charAt(4));
	    assertEquals("El primer elemento debería ser o",'o', in.charAt(5));
	    assertEquals("El primer elemento debería ser p",'p', pre.charAt(6));
	    
	}
	
	public void testContieneSubArbol1(){
		setupEscenario1();
		
		String[] preOrden = new String[6];
	    String[] inOrden = new String[6];
	    preOrden[0] = "U";
	    preOrden[1] = "A";
	    preOrden[2] = "D";
	    preOrden[3] = "E";
	    preOrden[4] = "N";
	    preOrden[5] = "S";
	    inOrden[0] = "D";
	    inOrden[1] = "A";
	    inOrden[2] = "E";
	    inOrden[3] = "U";
	    inOrden[4] = "N";
	    inOrden[5] = "S";
	    
	    arbol.reconstruirArbol(inOrden, preOrden, "UADENS", "DAEUNS");
	    
	    assertEquals("El árbol debería contener el subarbol", arbol.contieneSubArbol("ADE", "DAE"), true);
	    assertEquals("El árbol no debería contener el subarbol", arbol.contieneSubArbol("EDA", "EAD"), false);
	
	}
	
	public void testContieneSubArbol2(){
		setupEscenario1();
	    String[] preOrden = new String[7];
	    String[] inOrden = new String[7];
	    preOrden[0] = "a";
	    preOrden[1] = "b";
	    preOrden[2] = "d";
	    preOrden[3] = "e";
	    preOrden[4] = "c";
	    preOrden[5] = "f";
	    preOrden[6] = "g";
	    inOrden[0] = "d";
	    inOrden[1] = "b";
	    inOrden[2] = "e";
	    inOrden[3] = "a";
	    inOrden[4] = "f";
	    inOrden[5] = "c";
	    inOrden[6] = "g";
	    
	    arbol.reconstruirArbol(inOrden, preOrden, "abdecfg", "dbeafcg");
	    assertEquals("El árbol debería contener el subarbol",arbol.contieneSubArbol("cfg", "fcg"), true);
	    assertEquals("El árbol no debería contener el subarbol", arbol.contieneSubArbol("bcf", "cbf"), false);
	}
	
	public void testContieneSubArbol3(){
		setupEscenario1();
	    String[] preOrden = new String[7];
	    String[] inOrden = new String[7];
	    preOrden[0] = "j";
	    preOrden[1] = "k";
	    preOrden[2] = "l";
	    preOrden[3] = "o";
	    preOrden[4] = "q";
	    preOrden[5] = "h";
	    preOrden[6] = "p";
	    inOrden[0] = "k";
	    inOrden[1] = "l";
	    inOrden[2] = "j";
	    inOrden[3] = "h";
	    inOrden[4] = "q";
	    inOrden[5] = "o";
	    inOrden[6] = "p";
	    
	    arbol.reconstruirArbol(inOrden, preOrden, "jkloqhp", "kljhqop");
	    assertEquals("Debería contener el subarbol", arbol.contieneSubArbol("oqhp", "hqop"), true);
	    assertEquals("No debería contener el subarbol", arbol.contieneSubArbol("jkl", "kjl"), false);
	}
}
